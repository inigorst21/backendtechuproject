var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);
var should = chai.should();

var server = require('../index');

var idClient;
var token;

describe('Test obtener cuentas de usuario',
  function() {
    it('get token & idCliente', function(done) {
      chai.request('http://localhost:3000')
      .post("/backend/public/v1/login")
      .set('content-type', 'application/json')
      .send({"email":"11@11.com","password":"1111"})
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.email.should.be.eql("11@11.com");
        token = res.body.token;
        idClient = res.body.data.id;
        done();
      });
    }),it('get Client bank accounts & check lenght', function(done) {
      chai.request('http://localhost:3000')
      .get("/backend/private/v1/clients/" + idClient + "/accounts")
      .set('authorization', token)
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.should.be.a("array").length.above(1);
        token = res.body.token;
        done();
      });
    })
  });

describe('Test generar cuenta bancaria',
    function() {
      it('generar cuenta', function(done) {
        chai.request('http://localhost:3000')
        .post("/backend/private/v1/clients/" + idClient + "/accounts")
        .set('content-type', 'application/json')
        .set('authorization', token)
        .send({"balance":1000.0000})
        .end(function(err, res){
          res.should.have.status(201);
          res.body.should.have.property("token");
          done();
        });
      });
});
