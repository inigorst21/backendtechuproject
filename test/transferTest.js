var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);
var should = chai.should();

var server = require('../index');

var idClient;
var idAccount;
var token;

describe('Test obtener Transferencias de usuario',
  function() {
    it('get token & idCliente', function(done) {
      chai.request('http://localhost:3000')
      .post("/backend/public/v1/login")
      .set('content-type', 'application/json')
      .send({"email":"11@11.com","password":"1111"})
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.email.should.be.eql("11@11.com");
        token = res.body.token;
        idClient = res.body.data.id;
        done();
        })
      }), it('get Client bank accounts & check lenght', function(done) {
        chai.request('http://localhost:3000')
        .get("/backend/private/v1/clients/" + idClient + "/accounts")
        .set('authorization', token)
        .end(function(err, res){
          res.should.have.status(200);
          res.body.should.have.property("token");
          res.body.data.should.be.a("array").length.above(0);
          token = res.body.token;
          idAccount = res.body.data[0].id;
          done();
        })
    }), it('generar transferencia', function(done) {
            chai.request('http://localhost:3000')
            .post("/backend/private/v1/clients/" + idClient + "/accounts/" + idAccount + "/transfer")
            .set('content-type', 'application/json')
            .set('authorization', token)
            .send({ "account_id":idAccount,
                    "owner_id":idClient,
                    "IBAN": "ES8273 3665 35 192854067234",
                    "account_goal": "5b31231e5d0e650206bef658",
                    "owner_goal": "5af081e2ae2cce16d6fdc481",
                    "IBAN_GOAL": "ES2840 3319 32 924870384740 ",
                    "description": "npm Test Transfer",
                    "amount": 1234
                  })
            .end(function(err, res){
              res.should.have.status(201);
              res.body.should.have.property("token");
              done();
            })
          }), it('get bank tranfer & check lenght', function(done) {
      chai.request('http://localhost:3000')
      .get("/backend/private/v1/clients/" + idClient + "/accounts/" + idAccount + "/transfer")
      .set('authorization', token)
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.should.be.a("array").length.above(0);
        token = res.body.token;
        done();
      })
    });
  });
