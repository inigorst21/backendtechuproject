var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

var server = require('../index');

var idClient;
var accountId = "5b3b27f11f6e4f6fcc9108f1";
var token;
var coinAmount = 0;
var walletId;

describe('Test obtener datos publicos',
  function() {
    it('precio bitcoin', function(done) {
      chai.request('http://localhost:3000')
      .get("/backend/public/v1/coinValue/BTC")
      .set('content-type', 'application/json')
      .end(function(err, res){
        //console.log(res.body);
        res.should.have.status(200);
        //res.body.should.be.a("text");
        done();
      })
    });
});

describe('Test obtener wallets de cuenta',
  function() {
    it('login client test', function(done) {
      chai.request('http://localhost:3000')
      .post("/backend/public/v1/login")
      .set('content-type', 'application/json')
      .send({"email":"11@11.com","password":"1111"})
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.email.should.be.eql("11@11.com");
        token = res.body.token;
        idClient = res.body.data.id;
        done();
      })
    }),it('get wallets by account', function(done) {
      chai.request('http://localhost:3000')
      .get("/backend/private/v1/clients/" + idClient + "/accounts/" + accountId + "/wallets")
      .set('authorization', token)
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.should.be.a("array").length.above(1);
        token = res.body.token;
        coinAmount = res.body.data[0].balance;
        walletId = res.body.data[0].id;
        done();
      })
    })
});
describe('compra venta de bitcoins',
  function() {
    it('BUY 50 eur on bitcoins BTC', function(done) {
      chai.request('http://localhost:3000')
      .put("/backend/private/v1/clients/" + idClient + "/accounts/" + accountId + "/wallets")
      .set('authorization', token)
      .set('content-type', 'application/json')
      .send({	"id_account": accountId,
	            "id_wallet": walletId,
	            "coinType":"BTC",
	            "order":"buy",
	            "amount":50}
            )
      .end(function(err, res){
        res.should.have.status(201);
        res.body.should.have.property("token");
        done();
      });
    }),it('Sell 0.003 bitcoins BTC', function(done) {
      chai.request('http://localhost:3000')
      .put("/backend/private/v1/clients/" + idClient + "/accounts/" + accountId + "/wallets")
      .set('authorization', token)
      .set('content-type', 'application/json')
      .send({	"id_account": accountId,
	            "id_wallet": walletId,
	            "coinType":"BTC",
	            "order":"sell",
	            "amount":0.003}
            )
      .end(function(err, res){
        res.should.have.status(201);
        res.body.should.have.property("token");
        done();
      });
    });
});
