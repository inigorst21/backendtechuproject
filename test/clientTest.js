var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

var server = require('../index');

var idClient;
var token;
var clientName;
var emailClient;

describe('Test de Login API Clientes',
  function() {
    it('login client test', function(done) {
      chai.request('http://localhost:3000')
      .post("/backend/public/v1/login")
      .set('content-type', 'application/json')
      .send({"email":"11@11.com","password":"1111"})
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.email.should.be.eql("11@11.com");
        token = res.body.token;
        idClient = res.body.data.id;
        done();
      })
    }),it('get client details', function(done) {
      chai.request('http://localhost:3000')
      .get("/backend/private/v1/clients/" + idClient)
      .set('authorization', token)
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.email.should.be.eql("11@11.com");
        token = res.body.token;
        done();
      });
    });
});

describe('Test de actualizacion de datos de clientes', function(){
  it('update client details', function(done) {
    var ramdom = Math.floor(Math.random() * (20 - 1)) + 1;
    clientName = "Simbad" + ramdom;
    chai.request('http://localhost:3000')
    .put("/backend/private/v1/clients/" + idClient)
    .set('authorization', token)
    .send({"first_name":clientName})
    .end(function(err, res){
      res.should.have.status(200);
      res.body.should.have.property("token");
      token = res.body.token;
      done();
    })
  }),it('check update client details', function(done){
      chai.request('http://localhost:3000')
      .get("/backend/private/v1/clients/" + idClient)
      .set('authorization', token)
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.first_name.should.be.eql(clientName);
        token = res.body.token;
        done();
      });
    });
});


// Jesus
describe('Test de alta de nuevos clientes', function(){
    it('create existing client test', function(done) {
      var d = new Date();
      emailClient = "ClienteTest_"
                  +  d.getFullYear()
                  + (d.getMonth()+1)
                  + d.getDate()
                  + d.getHours()
                  + d.getMinutes()
                  + d.getSeconds()
                  + "@test.com";
//      console.log(emailClient);
      chai.request('http://localhost:3000')
      .post("/backend/public/v1/clients")
      .set('content-type', 'application/json')
      .send({"email":"11@11.com","password":"1111"})
      .end(function(err, res){
        res.should.have.status(200);
        done();
      })
    }),it('create client test', function(done) {
      chai.request('http://localhost:3000')
      .post("/backend/public/v1/clients")
      .set('content-type', 'application/json')
      .send({"email":emailClient,"password":"1111"})
      .end(function(err, res){
        res.should.have.status(201);
        done();
      });
    }),it('check client create client test', function(done) {
      chai.request('http://localhost:3000')
      .post("/backend/public/v1/login")
      .set('content-type', 'application/json')
      .send({"email":emailClient,"password":"1111"})
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.email.should.be.eql(emailClient);
        token = res.body.token;
        idClient = res.body.data.id;
        done();
      });
    });
});
