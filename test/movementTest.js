var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);
var should = chai.should();

var server = require('../index');

var idClient;
var idAccount;
var token;
var nowDate = new Date();

describe('Test obtener movimientos de usuario',
  function() {
    it('get token & idCliente', function(done) {
      chai.request('http://localhost:3000')
      .post("/backend/public/v1/login")
      .set('content-type', 'application/json')
      .send({"email":"11@11.com","password":"1111"})
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.email.should.be.eql("11@11.com");
        token = res.body.token;
        idClient = res.body.data.id;
        done();
        })
      }), it('get Client bank accounts & check lenght', function(done) {
        chai.request('http://localhost:3000')
        .get("/backend/private/v1/clients/" + idClient + "/accounts")
        .set('authorization', token)
        .end(function(err, res){
          res.should.have.status(200);
          res.body.should.have.property("token");
          res.body.data.should.be.a("array").length.above(0);
          token = res.body.token;
          idAccount = res.body.data[0].id;
          done();
        })
    }), it('generar movimiento', function(done) {
            chai.request('http://localhost:3000')
            .post("/backend/private/v1/clients/" + idClient + "/accounts/" + idAccount + "/movements")
            .set('content-type', 'application/json')
            .set('authorization', token)
            .send({ "account_id":idAccount,
                    "owner_id":idClient,
                    "IBAN":"ES8273 3665 35 192854067234",
                    "IBAN_GOAL":"ES2840 3319 32 924870384740",
                    "description":"Npm Test movement",
                    "amount":100,
                    "create_date" :  {"$date": nowDate},
                    "total":1000
                  })
            .end(function(err, res){
              res.should.have.status(201);
              res.body.should.have.property("token");
              done();
            })
          }), it('get bank movement & check lenght', function(done) {
      chai.request('http://localhost:3000')
      .get("/backend/private/v1/clients/" + idClient + "/accounts/" + idAccount + "/movements")
      .set('authorization', token)
      .end(function(err, res){
        res.should.have.status(200);
        res.body.should.have.property("token");
        res.body.data.should.be.a("array").length.above(1);
        token = res.body.token;
        done();
      })
    });
  });

describe('Test generar movimiento',
    function() {
      it('generar movimiento', function(done) {
        chai.request('http://localhost:3000')
        .post("/backend/private/v1/clients/" + idClient + "/accounts/" + idAccount + "/movements")
        .set('content-type', 'application/json')
        .set('authorization', token)
        .send({ "account_id" : idAccount,
                "owner_id" : idClient,
                "IBAN" : "ES8273 3665 35 192854067234",
                "IBAN_GOAL" : "ES2840 3319 32 924870384740",
                "description" : "Npm Test movement",
                "amount" : 1000,
                "create_date" :  {"$date": nowDate},
                "total" : 10000
              })
        .end(function(err, res){
          res.should.have.status(201);
          res.body.should.have.property("token");
          done();
        })
      });
});
