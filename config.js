module.exports = {
  TOKEN_SECRET: process.env.TOKEN_SECRET || "s1M3L33sT3R3v13nt0",
  MLAB_URL: "https://api.mlab.com/api/1/databases/apitechu_inigo/collections/",
  MLAB_KEY: "apiKey=Tu_apx1h-YokfwcIdJvsBW0IlhiINBCJ",
  MLAB_CLIENT_COLLECTION: "user",
  MLAB_ACCOUNT_COLLECTION: "account",
  MLAB_TRANSFER_COLLECTION: "transfer",
  COINGY_PUB: "c16ebba02b7d9ff013eb79f0ad238940",
  COINGY_PRI: "cba692398b12399bb048fabfb999263c",
  MLAB_MOVEMENT_COLLECTION: "movements",
  MLAB_WALLET_COLLECTION: "wallets",
  MLAB_HISTORICAL_COLLECTION: "historicalPrices",
  CRIPTOCOMPARE: true,
  CRIPTOCOMPARE_URL: "https://min-api.cryptocompare.com/data/"
};
