var config = require('../config');
var request = require('request-promise');

exports.getCoinValue = function(coinType, next){
if(!config.CRIPTOCOMPARE){
  var body =   {
        exchange_code : "GDAX",
        exchange_market : coinType + "/EUR"
    };
  request({
    "method":"POST",
    "uri": "https://api.coinigy.com/api/v1/ticker",
    "body": body,
    "headers": {'X-API-KEY': config.COINGY_PUB ,'X-API-SECRET': config.COINGY_PRI},
    "json": true,
  }).then(function(result){
  //  console.log(result);
    next(result.data[0].last_trade , 200);
  }).catch(function (err) {
    console.log(err);
    next(err, 500);
  });
}else{
  //console.log(config.CRIPTOCOMPARE_URL + "price?fsym=" + coinType + "&tsyms=EUR");
  request({
    "method":"GET",
    "uri": config.CRIPTOCOMPARE_URL + "price?fsym=" + coinType + "&tsyms=EUR",
    "json": true,
  }).then(function(result){
    next(result.EUR, 200);
  }).catch(function (err) {
    next(err, 500);
  });
}
};


exports.getCoinValueByTimestamp = function(coinData, next){
  request({
    "method":"GET",
    "uri": config.CRIPTOCOMPARE_URL + "pricehistorical?fsym=" + coinData.coinType + "&tsyms=EUR&ts=" +coinData.datetime,
    "json": true,
  }).then(function(result){
    if (!('Response' in result)){
      coinData.value = result[coinData.coinType].EUR;
      next(coinData , 200);
    }else{
      next(coinData, 429);
    }
  }).catch(function (err) {
    next(err, 500);
  });
};
