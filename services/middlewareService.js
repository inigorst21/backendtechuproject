var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config');

exports.createToken = function(clientID) {
  var payload = {
    sub: clientID,
    iat: moment().unix(),
    exp: moment().add(100000, "minutes").unix(),
  };
  //cambiar a 10 minutos
  return jwt.encode(payload, config.TOKEN_SECRET);
};

exports.ensureAuthenticated = function(req, res, next) {
  if(!req.headers.authorization) {
    return res
      .status(403)
      .send({message: "Tu petición no tiene cabecera de autorización"});
  }

  try{
    var payload = jwt.decode(req.headers.authorization, config.TOKEN_SECRET);
  }catch (err) {
    return res.status(403).send({message: "El token no es valido"});
  }

  if(payload.exp <= moment().unix()) {
     return res
     	.status(401)
        .send({message: "El token ha expirado"});
  }

  req.clientID = payload.sub;
  req.params.id
  if(req.clientID != req.params.id){
    return res.status(403)
      .send({message: "Tu usuario no tiene permisos de acceso"});
  }
  next();
}
