'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var client_controller = require('./controllers/clientController');
var account_controller = require('./controllers/accountController');
var movement_controller = require('./controllers/movementController');
var transfer_controller = require('./controllers/transferController');
var wallet_controller = require('./controllers/walletController');
var middleware = require('./services/middlewareService');

var app = express();
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en puerto " + port);
var routerPrivate = express.Router();
var routerPublic = express.Router();


///Client API public///
routerPublic.post('/v1/login', client_controller.client_login_post);
routerPublic.post('/v1/clients', client_controller.client_clientData_post);
routerPublic.get('/v1/coinValue/:coin', wallet_controller.wallet_coinPrice_get);
routerPublic.post('/v1/coinValue/:coin/update', wallet_controller.wallet_coinUpdatePrice_post);
routerPublic.get('/v1/clients',  client_controller.client_clientsData_get);
routerPublic.get('/v1/clients/:id/accounts',  account_controller.account_listAccounts_get);
routerPublic.get('/v1/coinValue/:coin/historical', wallet_controller.wallet_coinHistoricalPrice_get);
///Client API private///
routerPrivate.get('/v1/clients/:id', middleware.ensureAuthenticated, client_controller.client_clientData_get);
routerPrivate.put('/v1/clients/:id', middleware.ensureAuthenticated, client_controller.client_clientData_put);
///Account API private///
routerPrivate.get('/v1/clients/:id/accounts', middleware.ensureAuthenticated, account_controller.account_myAccounts_get);
routerPrivate.post('/v1/clients/:id/accounts', middleware.ensureAuthenticated, account_controller.account_accountData_post);
///Movements API private///
routerPrivate.get('/v1/clients/:id/movements', middleware.ensureAuthenticated, movement_controller.movement_clientMovemenstData_get);
routerPrivate.get('/v1/clients/:id/accounts/:account_id/movements', middleware.ensureAuthenticated, movement_controller.movement_accountMovemenstData_get);
routerPrivate.get('/v1/clients/:id/accounts/:account_id/lastmovement', middleware.ensureAuthenticated, movement_controller.movement_lastMovementData_get);
routerPrivate.get('/v1/clients/:id/accounts/:account_id/movements/:movement_id', middleware.ensureAuthenticated, movement_controller.movement_movementData_get);
routerPrivate.put('/v1/clients/:id/accounts/:account_id/movements/:movement_id', middleware.ensureAuthenticated, movement_controller.movement_movementData_put);
routerPrivate.post('/v1/clients/:id/accounts/:account_id/movements', middleware.ensureAuthenticated, movement_controller.movement_movementData_post);
///Transfer API private///
routerPrivate.get('/v1/clients/:id/accounts/:account_id/transfer', middleware.ensureAuthenticated, transfer_controller.transfer_transferData_get);
routerPrivate.post('/v1/clients/:id/accounts/:account_id/transfer', middleware.ensureAuthenticated, transfer_controller.transfer_transferData_post);

///Wallet private///
routerPrivate.get('/v1/clients/:id/accounts/:accountId/wallets', middleware.ensureAuthenticated, wallet_controller.wallet_accountWallets_get);
routerPrivate.post('/v1/clients/:id/accounts/:accountId/wallets', middleware.ensureAuthenticated, wallet_controller.wallet_accountWallets_post);
routerPrivate.put('/v1/clients/:id/accounts/:accountId/wallets', wallet_controller.wallet_buySellCoins_put);

// mount the router on the app
app.use('/backend/private', routerPrivate);
app.use('/backend/public', routerPublic);
//module.exports = router;
