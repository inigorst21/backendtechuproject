var config = require('../config');
var request = require('request-promise');

exports.searchByParams = function(params, response) {
  var query = "q=" + params;
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_WALLET_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    var wallets=[];
    for (let i = 0; i < result.length; i++) {
      var wallet = {
        balance: result[i].balance,
        coinType: result[i].coinType,
        walletAddress: "https://savelessbank.com/wallet/" + result[i].walletAddress,
        id: result[i]._id.$oid
      }
      wallets.push(wallet);
    }
    response(wallets, 200);
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};

exports.create = function(wallet, response){
  request({
    "method":"POST",
    "uri": config.MLAB_URL + config.MLAB_WALLET_COLLECTION +"?" + config.MLAB_KEY,
    "body": wallet,
    "json": true,
  }).then(function(result){
//    console.log("funcion de creacion");
    response(result, 200);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
};

exports.updateById = function(id, value, response) {
  var query = 'q={"_id" :{"$oid" : "' + id + '"}}';
  var putBody = '{"$set":' + JSON.stringify(value) + '}';
  request({
    "method":"PUT",
    "body": JSON.parse(putBody),
    "uri": config.MLAB_URL + config.MLAB_WALLET_COLLECTION + "?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    response({"msg":"wallet actualizado"}, 200);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
};
