var config = require('../config');
var request = require('request-promise');

exports.searchByParams = function(params, response) {
  var query = "q=" + params;
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_ACCOUNT_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    var accounts=[];
    for (let i = 0; i < result.length; i++) {
      var myAccount = {
        IBAN: result[i].IBAN,
        id: result[i]._id.$oid,
        balance: result[i].balance
      }
      accounts.push(myAccount);
    }
    response(accounts, 200);
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};


exports.searchAll = function(params, response) {
  var query = "q=" + params + "&l=4";  
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_ACCOUNT_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    if (result.length > 0) {
      var users=[];
      for (let i = 0; i < result.length; i++) {
        var user = {
          IBAN: result[i].IBAN,
          id: result[i]._id.$oid
        };
        users.push(user);
    }
    //  var movement = result;
      response(users, 200);
    } else {
      response(users, 204);
    }
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};

exports.updateBalanceById = function(id_account ,value ,response){
  var query = 'q={"_id" :{"$oid" : "' + id_account + '"}}';
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_ACCOUNT_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    var balanceTotal = result[0].balance + value;
    var myAccount = {
      IBAN: result[0].IBAN,
      id: result[0]._id.$oid,
      balance: balanceTotal
    }
    var balanceBody = '{"$set":{"balance" :' + balanceTotal + '}}';
    request({
      "method":"PUT",
      "body": JSON.parse(balanceBody),
      "uri": config.MLAB_URL + config.MLAB_ACCOUNT_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
      "json": true,
    }).then(function(result){
      response(myAccount, 200);
    }).catch(function (err) {
      console.log(err);
      response('{"msg":'+err+'}', 500);
    });
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};


exports.addNew = function(newAccount, response) {
  var putBody = JSON.stringify(newAccount);
  request({
    "method":"POST",
    "body": JSON.parse(putBody),
    "uri": config.MLAB_URL + config.MLAB_ACCOUNT_COLLECTION +"?" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    response({"msg":"cuenta insertado"}, 201);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
};
