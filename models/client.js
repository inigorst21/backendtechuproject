var config = require('../config');
var request = require('request-promise');




exports.searchByParams = function(params, response) {
  var query = "q=" + params;
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_CLIENT_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    if (result.length > 0) {
      var client = {
        first_name: result[0].first_name,
        last_name: result[0].last_name,
        email: result[0].email,
        id: result[0]._id.$oid,
        photo: result[0].photo
      };
      response(client, 200);
    } else {
      response(client, 204);
    }
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};

exports.searchAll = function(params, response) {
  var query = "l=4";
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_CLIENT_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    if (result.length > 0) {
      var users=[];
      for (let i = 0; i < result.length; i++) {
        var user = {
          first_name: result[i].first_name,
          last_name: result[i].last_name,
          id: result[i]._id.$oid
        };
        users.push(user);
    }
    //  var movement = result;
      response(users, 200);
    } else {
      response(users, 204);
    }
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};

exports.updateById = function(id, client, response) {
  var query = 'q={"_id" :{"$oid" : "' + id + '"}}';
  var putBody = '{"$set":' + JSON.stringify(client) + '}';
  request({
    "method":"PUT",
    "body": JSON.parse(putBody),
    "uri": config.MLAB_URL + config.MLAB_CLIENT_COLLECTION + "?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    response({"msg":"usuario actualizado"}, 200);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
};

//jesus

exports.addNew = function(newClient, response) {
  var putBody = JSON.stringify(newClient);
  request({
    "method":"POST",
    "body": JSON.parse(putBody),
    "uri": config.MLAB_URL + config.MLAB_CLIENT_COLLECTION +"?" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    response({"msg":"usuario insertado"}, 201);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
};
