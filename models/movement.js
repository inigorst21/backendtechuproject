var config = require('../config');
var request = require('request-promise');
var moment = require('moment');




exports.searchByParams = function(params, response) {
  var query = "q=" + params;

  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_MOVEMENT_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    if (result.length > 0) {
      var movement = {
        owner_id: result[0].owner_id,
        account_id: result[0].account_id,
        iban: result[0].IBAN,
        iban_goal: result[0].IBAN_GOAL,
        id: result[0]._id.$oid,
        description: result[0].description,
        create_date: moment(result[0].create_date, 'YYYY-MM-DD HH:mm:ss').format("DD-MM-YYYY HH:mm"),
        amount: result[0].amount,
        total: result[0].total
      };
      response(movement, 200);
    } else {
      response(movement, 204);
    }
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};


exports.searchArrayByParams = function(params, response) {
  var query = "q=" + params;
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_MOVEMENT_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    if (result.length > 0) {
      var movements=[];
      for (let i = 0; i < result.length; i++) {
        if(result[i].create_date !== undefined){
    //      console.log(result[i].create_date.$date);
          var movement = {
            owner_id: result[i].owner_id,
            account_id: result[i].account_id,
            iban: result[i].IBAN,
            iban_goal: result[i].IBAN_GOAL,
            id: result[i]._id.$oid,
            description: result[i].description,
            create_date: moment(result[i].create_date.$date).format("DD-MM-YYYY HH:mm"),
            sort_date:  moment(result[i].create_date.$date).format("YYYYMMDDHHmm"),
            amount: result[i].amount,
            total: result[i].total
          };
          //console.log(result[i].create_date);
          //console.log(movement);
          movements.push(movement);
        }else{
        console.log("error");
      }
    }
    //  var movement = result;
      response(movements, 200);
    } else {
      response(result, 204);
    }
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};


exports.updateById = function(id, client, response) {
  var query = 'q={"_id" :{"$oid" : "' + id + '"}}';
  var putBody = '{"$set":' + JSON.stringify(client) + '}';
  request({
    "method":"PUT",
    "body": JSON.parse(putBody),
    "uri": config.MLAB_URL + config.MLAB_MOVEMENT_COLLECTION + "?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    response({"msg":"usuario actualizado"}, 200);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
};



exports.addNew = function(newMovement, response) {
  var putBody = JSON.stringify(newMovement);
  request({
    "method":"POST",
    "body": JSON.parse(putBody),
    "uri": config.MLAB_URL + config.MLAB_MOVEMENT_COLLECTION +"?" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    response({"msg":"movimiento insertado"}, 201);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
};
