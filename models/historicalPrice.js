var request = require('request-promise');
var config = require('../config');
var moment = require('moment');


exports.searchByParamsSort = function(queryParams, sortParams, response) {
  var query = "q=" + queryParams;
  var sort = "&s=" + sortParams;
  var chartDataResults = [];
  console.log(config.MLAB_URL + config.MLAB_HISTORICAL_COLLECTION +"?" + query + sort +
  '&f={"value":1,"date":1,"datetime":1,"coinType":1,"_id":0} &' + config.MLAB_KEY);
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_HISTORICAL_COLLECTION +"?" + query + sort +
    '&f={"value":1,"date":1,"datetime":1,"coinType":1,"_id":0} &' + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
      for(var i=0; i<result.length; i++){
        var timestamp = moment(result[i].date.$date).valueOf();
        var chartData =[timestamp , result[i].value];
        chartDataResults.push(chartData);
      }
      response(chartDataResults, 200);
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};

exports.searchByParamsSortLimit = function(queryParams, sortParams, limitParam, response) {
  var query = "q=" + queryParams;
  var sort = "&s=" + sortParams;
  var limit = "&l=" + limitParam;
  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_HISTORICAL_COLLECTION +"?" + query + sort + limit + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
  //  console.log(result);
      response(result, 200);
  }).catch(function (err) {
      console.log(err);
    response('{"msg":' + err + '}', 500);
  });
};

exports.addNew = function(data, response) {
  var putBody = JSON.stringify(data);
  console.log("save: "+ putBody);
  request({
    "method":"POST",
    "body": JSON.parse(putBody),
    "uri": config.MLAB_URL + config.MLAB_HISTORICAL_COLLECTION +"?" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    response({"msg":"datos insertado"}, 201);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
}
