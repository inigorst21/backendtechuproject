var config = require('../config');
var request = require('request-promise');




exports.searchByParams = function(params, response) {
  var query = "q=" + params;

  request({
    "method":"GET",
    "uri": config.MLAB_URL + config.MLAB_TRANSFER_COLLECTION +"?" + query + "&" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    if (result.length > 0) {
      var client = result;
      response(client, 200);
    } else {
      response(client, 204);
    }
  }).catch(function (err) {
    response('{"msg":' + err + '}', 500);
  });
};


exports.addNew = function(newTransfer, response) {
  var putBody = JSON.stringify(newTransfer);
  request({
    "method":"POST",
    "body": JSON.parse(putBody),
    "uri": config.MLAB_URL + config.MLAB_TRANSFER_COLLECTION +"?" + config.MLAB_KEY,
    "json": true,
  }).then(function(result){
    response({"msg":"Transferencia realizada"}, 201);
  }).catch(function (err) {
    console.log(err);
    response('{"msg":'+err+'}', 500);
  });
};
