var transferModel = require('../models/transfer');
var movementModel = require('../models/movement');
var accountModel = require ('../models/account');
var serviceMiddleware = require('../services/middlewareService');


exports.transfer_transferData_get = function(req, res) {
    transferModel.searchByParams('{"owner_id" : "' + req.params.id + '" , $and: [ { "account_id": "' + req.params.account_id + '" } ] }', function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});
  });
};

exports.transfer_transferData_post = function(req, res){
  var nowDate = new Date();
  var newTransfer = {
      "account_id" : req.body.account_id,
      "owner_id" : req.body.owner_id,
      "IBAN" : req.body.IBAN,
      "account_goal" : req.body.account_goal,
      "owner_goal" : req.body.owner_goal,
      "IBAN_GOAL" : req.body.IBAN_GOAL,
      "description" : req.body.description,
      "create_date" :  {"$date": nowDate},
      "amount" : req.body.amount
  };
  transferModel.addNew(newTransfer, function(result, statusTransfer){
    res.status(statusTransfer);

    if (statusTransfer == 201) {
      accountModel.updateBalanceById(req.body.account_id, -1 *  Number(req.body.amount) , function(resultBalance1, statusBalance1){
        res.status(statusBalance1);
        //console.log("1");
        if (statusBalance1 == 200) {
          //console.log("2");
          var newMovementDebe = {
              "account_id" : req.body.account_id,
              "owner_id" : req.body.owner_id,
              "IBAN" : req.body.IBAN,
              "IBAN_GOAL" : req.body.IBAN_GOAL,
              "description" : "Tranferencia Realizada: " + req.body.description,
              "amount" : -1 * req.body.amount,
              "create_date" :  {"$date": nowDate},
              "total" : resultBalance1.balance
          };
          movementModel.addNew(newMovementDebe, function(result, statusDebe){
            res.status(statusDebe);
            //console.log("3");
            if (statusDebe == 201) {
              //console.log("4");
              accountModel.updateBalanceById(req.body.account_goal, Number(req.body.amount) , function(resultBalance2, statusBalance2){
                res.status(statusBalance2);
                console.log(resultBalance2);
                var newMovementHaber = {
                    "account_id" : req.body.account_goal,
                    "owner_id" : req.body.owner_goal,
                    "IBAN" : req.body.IBAN_GOAL,
                    "IBAN_GOAL" : req.body.IBAN,
                    "description" : "Tranferencia Recibida: " + req.body.description,
                    "amount" : req.body.amount,
                    "create_date" :  {"$date": nowDate},
                    "total" : resultBalance2.balance
                };
                //console.log("5");
                movementModel.addNew(newMovementHaber, function(result, statusHaber){
                  //console.log("6");
                  res.status(statusHaber);
                //  res.set('token', serviceMiddleware.createToken(req.clientID));
                  res.set('Content-Type', 'aplication/json');
                  res.send({"token":serviceMiddleware.createToken(req.clientID),"data":{"msg":"Transferencia Realizada"}});
                });
              });
            } else {
              //console.log("7");
            //  res.set('token', serviceMiddleware.createToken(req.clientID));
              res.status(statusDebe);
              res.set('Content-Type', 'aplication/json');
              res.send({"msg":"No se pudo realizar la Transferencia"});
            };
          });
      }
    });
  } else {
    //console.log("8");
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.status(statusTransfer);
    res.set('Content-Type', 'aplication/json');
    res.send({"msg":"No se pudo realizar la Transferencia"});
    }
  });
};
