var walletModel = require('../models/wallet');
var accountModel = require('../models/account');
var serviceMiddleware = require('../services/middlewareService');
var coinService = require('../services/coinValueService');
var movementModel = require('../models/movement');
var historicalModel = require('../models/historicalPrice');
var moment = require('moment');

exports.wallet_accountWallets_get = function(req, res) {
  walletModel.searchByParams('{"id_account":"' + req.params.accountId + '"}', function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});
  });
};

exports.wallet_accountWallets_post = function(req, res){
//  console.log(req.body);
//  res.set('token', serviceMiddleware.createToken(req.clientID));
  res.set('Content-Type', 'aplication/json');
  walletModel.searchByParams('{"id_account":"' + req.body.id_account + '","coinType":"' + req.body.coinType + '"}', function(result, status){
    if(result.length == 0 && status == 200){
      var randomstring = require("randomstring");
      var wallet = {
        balance: parseFloat(0),
        coinType: req.body.coinType,
        walletAddress: randomstring.generate(32),
        id_account: req.body.id_account
      };
      walletModel.create(wallet, function(result, status){
        res.status(status);
        res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});
      });
    }else{
      if(status == 200){
        res.status(409);
        res.send({"msg":"el wallet ya existe para esa cuenta"});
      }
      res.status(status);
      res.send(JSON.stringify(result));
    }
  });
};

exports.wallet_coinPrice_get = function(req, res){
  coinService.getCoinValue(req.params.coin, function(price, status){
    res.status(status);
    res.set('Content-Type', 'aplication/text');
    res.send(price.toString());
  });
};

/*obtener historico de precios sobre dos fechas y determinada moneda*/
exports.wallet_coinHistoricalPrice_get = function(req, res){
  var coinType = req.params.coin;
  var param ='{"coinType":"' + coinType + '"';
  if(req.query.start !== undefined){
    var dateStart = moment(req.query.start, "DD-MM-YYYY").format('YYYY-MM-DD');
    param += ',"$and" : [{"date" : {"$gte" : "' + dateStart + '"}}';
  }if(dateEnd !== undefined){
    var dateEnd = moment(req.query.end, "DD-MM-YYYY").format('YYYY-MM-DD');
    param +=', {"date" : {"$lte" : "' + dateEnd + '"}}';
  }if(req.query.start !== undefined || req.query.end !== undefined){
    param += ']';
  }
  param += '}';
  console.log(param);
  var sort = '{"date": -1}';
  historicalModel.searchByParamsSort(param,sort, function(result, status){
    res.status(status);
    res.set('Content-Type', 'aplication/json');
    res.send(result);
  });
}

/*actualizar historico de precios de una determinada moneda*/
exports.wallet_coinUpdatePrice_post = function(req, res){
  var coinType = req.params.coin;
  var param ='{"coinType":"'+coinType+'"}';
  var sort = '{"date": -1}';
  historicalModel.searchByParamsSortLimit(param,sort,1,function(result, status){
    console.log(status);

  if(result.length>0){

    if(status == 200){

      var desde = moment(result[0].date.$date);
      var hasta = moment();
      var dia_actual = desde;
      var fechas = [];
      console.log(desde);
      getDataFromOutside(dia_actual, hasta, coinType);
    }
  }else{
    res.status(status);
    res.set('Content-Type', 'aplication/text');
    res.send("No hay actualizaciones pendientes");
  }
  });
}

function getDataFromOutside(dia_actual, hasta, coinType){
  var dataHistoricalCoin = {
    "value" : 0,
    "datetime" : dia_actual.unix().toString(),
    "coinType" : coinType,
    "date" : {"$date": dia_actual }
  };
  coinService.getCoinValueByTimestamp(dataHistoricalCoin , function(coinData, status){
    if(status == 200){
      console.log("resultado: "+ coinData);

      historicalModel.addNew(coinData, function(msg, status){
        if(status == 500){
          res.status(status);
          res.set('Content-Type', 'aplication/text');
          res.send("error de almacenado del dato en fecha " + coinData.date);
        }
      });
      if(dia_actual.isBefore(hasta)){
        getDataFromOutside(dia_actual.add(1, 'days'), hasta, coinType);
      }else{
        res.status(200);
        res.set('Content-Type', 'aplication/text');
        res.send("se ha actualizado todo muy bien");
      }
    }if(status == 429){
      res.status(429);
      res.set('Content-Type', 'aplication/text');
      res.send("No se han podido actualizar todos los datos , limite excedido en " + coinData.date);
    }
  });
};

exports.wallet_buySellCoins_put = function(req, res){
  var order = req.body.order;
  var account_id = req.body.id_account;
  var amount = req.body.amount;
  var clientIdentifier = req.body.id_client;
//  console.log("amount: " + amount);
  coinService.getCoinValue(req.body.coinType, function(price, status){
    if(status == 200){
//      console.log("coin price: " + price);
      if(order == "sell"){
        walletModel.searchByParams('{"_id" :{"$oid" : "' + req.body.id_wallet + '"}}', function(wallet, status){
          if(status == 200){
//          console.log("se dispone de : " + wallet[0].balance + " coins");
            if(wallet[0].balance >= amount){
              walletModel.updateById(req.body.id_wallet, {"balance" : wallet[0].balance-amount}, function(result,status){
                accountModel.updateBalanceById(account_id, price * amount, function(account, status){
//                  console.log("se añade a cuenta "+ price * amount + "euros");
                  var nowDate = new Date();
                  var newMovement = {
                      "account_id" : account_id,
                      "owner_id" : clientIdentifier,
                      "IBAN" : "Wallet_" + req.body.coinType + "_" + wallet[0].id,
                      "IBAN_GOAL" : account.IBAN,
                      "description" : "venta " + req.body.coinType + " " + amount +" coins",
                      "amount" : price * amount,
                      "create_date" : {"$date": nowDate},
                      "total" : account.balance
                  };
//                  console.log(newMovement);
                  movementModel.addNew(newMovement, function(result, status){
//                    console.log("has vendido : " + amount + " " + req.body.coinType + " por : " + price * amount + " euros");
                    res.status(status);
                  //  res.set('token', serviceMiddleware.createToken(req.clientID));
                    res.set('Content-Type', 'aplication/json');
                    res.send({'token': serviceMiddleware.createToken(req.clientID),"data":{"msg":"has vendido : " + amount + " " + req.body.coinType + " por : " + price * amount + " euros"}});
                  });
                });
              });
            }else{
//              console.log("cantidad de coins superior a los disponibles");
              res.status(409);
              res.send("cantidad de coins superior a los disponibles");
            }
          }
        });
      }if(order =="buy"){
        walletModel.searchByParams('{"_id" :{"$oid" : "' + req.body.id_wallet + '"}}', function(wallet, status){
          if(status == 200){
              walletModel.updateById(req.body.id_wallet, {"balance" : wallet[0].balance + (req.body.amount / price) }, function(result,status){
                accountModel.updateBalanceById(account_id, -amount , function(account, status){
                //  console.log("se añade a cuenta "+ price * amount + "euros");
                  var nowDate = new Date();
                  var newMovement = {
                      "account_id" : account_id,
                      "owner_id" : clientIdentifier,
                      "IBAN" : account.IBAN,
                      "IBAN_GOAL" : "Wallet_" + req.body.coinType + "_" + wallet[0].id,
                      "description" : "compra " + req.body.coinType + " " + req.body.amount / price +" coins",
                      "amount" : -amount,
                      "create_date" : {"$date": nowDate},
                      "total" : account.balance
                  };
  //                console.log(newMovement);
                  movementModel.addNew(newMovement, function(result, status){
  //                  console.log("Has comprado : " + req.body.amount / price + " " + req.body.coinType + " coins");
                    res.status(status);
                  //  res.set('token', serviceMiddleware.createToken(req.clientID));
                    res.set('Content-Type', 'aplication/json');
                    res.send({'token': serviceMiddleware.createToken(req.clientID),"data":{"msg":"Has comprado : " + req.body.amount / price + " " + req.body.coinType + " coins por "+ req.body.amount + " euros"}});
                  });
                });
              });
          }
        });
      }
    }
  //  res.status(status);
  //  res.send(price);
  });

};
