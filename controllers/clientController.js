var clientModel = require('../models/client');
var serviceMiddleware = require('../services/middlewareService');

exports.client_login_post = function(req, res) {
    var params = '{"email":"' + req.body.email + '","password":"' + req.body.password +'"}';
    clientModel.searchByParams(params, function(result, status){
      if(result !== undefined ){
        res.status(status);
        //res.set('token', serviceMiddleware.createToken(result.id));
        res.set('Content-Type', 'aplication/json');
        res.send({"token":serviceMiddleware.createToken(result.id),"data":result});
      }else{
        res.status(401);
        res.set('Content-Type', 'aplication/json');
        res.send(JSON.stringify({"msg":"usuario password invalido"}));
      }
    });
};

exports.client_clientData_get = function(req, res) {
  clientModel.searchByParams('{"_id" :{"$oid" : "' + req.params.id + '"}}', function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token" : serviceMiddleware.createToken(req.clientID) , "data" : result});
  });
};

exports.client_clientsData_get = function(req, res) {
  clientModel.searchAll('{"1" :"1"}', function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
   res.send(JSON.stringify(result));
  });
};

exports.client_clientData_put = function(req, res){
  clientModel.updateById(req.params.id,req.body, function(result, status){
    res.status(status);
//    res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});
  });
};


//Jesus

exports.client_clientData_post = function(req, res){
  var params = '{"email":"' + req.body.email + '","password":"' + req.body.password +'"}';
  clientModel.searchByParams(params, function(resultExist, statusExist){

    res.status(statusExist);
    if (statusExist == 204) {
      var newUser = {
          "email" : req.body.email,
          "password" : req.body.password,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name
      };
      clientModel.addNew(newUser, function(result, status){
        res.status(status);
        res.set('Content-Type', 'aplication/json');
        res.send(JSON.stringify(result));

      });
    } else {
      res.status(statusExist);
      res.set('Content-Type', 'aplication/json');
      res.send({"msg":"usuario no se pudo dar de alta"})
    }
  });
};
