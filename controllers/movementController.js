var movementModel = require('../models/movement');
var accountModel = require ('../models/account');
var serviceMiddleware = require('../services/middlewareService');
var moment = require('moment');

exports.movement_movementData_get = function(req, res) {
    movementModel.searchByParams('{"owner_id" : "' + req.params.id + '" , $and: [ { "account_id": "' + req.params.account_id + '" },{"_id" :{"$oid" : "' + req.params.movement_id + '"}} ] }&s={"create_date": 1}', function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(result.id),"data":result});
  });
};

exports.movement_lastMovementData_get = function(req, res) {
    movementModel.searchArrayByParams('{"owner_id" : "' + req.params.id + '" , $and: [ { "account_id": "' + req.params.account_id + '" } ] }&s={"create_date": -1}&l=1', function(result, status){
    res.status(status);
//    res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});
  });
};


exports.movement_accountMovemenstData_get = function(req, res) {
    movementModel.searchArrayByParams('{"owner_id" : "' + req.params.id + '" , $and: [ { "account_id": "' + req.params.account_id + '" } ] }&s={"create_date": 1}', function(result, status){
    res.status(status);
//    res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});
  });
};

exports.movement_clientMovemenstData_get = function(req, res) {
    movementModel.searchArrayByParams('{"owner_id" : "' + req.params.id + '"}&s={"create_date": 1}', function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});
  });
};

exports.movement_movementData_put = function(req, res){
    movementModel.updateById(req.params.movement_id,req.body, function(result, status){
    res.status(status);
//    res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});
  });
};


exports.movement_movementData_post = function(req, res){
  var params = '{"owner_id" : "' + req.params.id + '" , $and: [ { "account_id": "' + req.params.account_id + '" } ] }&s={"create_date": -1}&l=1';
  accountModel.updateBalanceById(req.params.account_id, Number(req.body.amount) , function(resultExist, statusExist){
    res.status(statusExist);
//    console.log(resultExist);
    if (statusExist == 200) {
      var nowDate = new Date();
      var newMovement = {
          "account_id" : req.body.account_id,
          "owner_id" : req.body.owner_id,
          "IBAN" : req.body.IBAN,
          "IBAN_GOAL" : req.body.IBAN_GOAL,
          "create_date" :  {"$date": nowDate},
          "description" : req.body.description,
          "amount" : req.body.amount,
          "total" : resultExist.balance
      };
    //  console.log(newMovement);
      movementModel.addNew(newMovement, function(result, status){
        res.status(status);
    //    res.set('token', serviceMiddleware.createToken(req.clientID));
        res.set('Content-Type', 'aplication/json');
        res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});

      });
    } else {
      res.status(statusExist);
      res.set('Content-Type', 'aplication/json');
      res.send({"msg":"movimiento no se pudo dar de alta"})
    }
  });
};
