var accountModel = require('../models/account');
var serviceMiddleware = require('../services/middlewareService');

exports.account_myAccounts_get = function(req, res) {
  accountModel.searchByParams('{"owner_id":"' + req.params.id + '"}}', function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token" : serviceMiddleware.createToken(req.clientID) , "data":result});
  });
};

exports.account_listAccounts_get = function(req, res) {
  accountModel.searchAll('{"owner_id":"' + req.params.id + '"}}', function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token" : serviceMiddleware.createToken(req.clientID) , "data":result});
  });
};

exports.account_accountData_post = function(req, res){

  function generateIBAN(){
    return "ES" + Math.floor(Math.random() * (9999 - 1000) + 1000) + " " +
    Math.floor(Math.random() * (9999 - 1000) + 1000) + " " + Math.floor(Math.random() * (99 - 10) + 10) + " " +
    Math.floor(Math.random() * (999999999999 - 100000000000) + 100000000000);
  }

  var newAccount = {
      "owner_id" : req.params.id,
      "IBAN" : generateIBAN(),
      "balance" : req.body.balance
  };
  accountModel.addNew(newAccount, function(result, status){
    res.status(status);
  //  res.set('token', serviceMiddleware.createToken(req.clientID));
    res.set('Content-Type', 'aplication/json');
    res.send({"token":serviceMiddleware.createToken(req.clientID),"data":result});

  });
};
